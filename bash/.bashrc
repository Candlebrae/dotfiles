# https://codeberg.org/Owlrooster/dotfiles
# owlsroost@riseup.net
# OS: Linux
# Optional dependencies: fortune, bash-completion

# ------------------------ Shell tweaks ------------------------

### Set PATH so it includes private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

### Enable bash completion
[ -f /etc/bash_completion ] && . /etc/bash_completion

### Enable blesh https://github.com/akinomyoga/ble.sh
[ -f ~/.local/share/blesh/ble.sh ] && source ~/.local/share/blesh/ble.sh

### If not running interactively, don't do anything (from base Debian config)
[[ $- != *i* ]] && return

### Check the window size after each command.
shopt -s checkwinsize

### Set prompt.
# one line alt: 
# PS1='\[\033[01;34m\][\u@\h \W]\[\033[00m\]\$ '
# two line default:
PS1='\[\033[01;34m\]\s \v, cmd \!\n\t \w \$\[\033[00m\] '

### Disable CTRL-S and CTRL-Q
[[ $- =~ i ]] && stty -ixoff -ixon

### Colorful MAN pages (from BasilGuo: https://github.com/BasilGuo/dotfiles/blob/master/.bashrc)
export LESS_TERMCAP_md=$'\e[01;31m'   # Start bold effect (double-bright).
export LESS_TERMCAP_me=$'\e[0m'       # Stop bold effect.
export LESS_TERMCAP_us=$'\e[1;4;32m'  # Start underline effect.
export LESS_TERMCAP_ue=$'\e[0m'       # Stop underline effect.
export LESS_TERMCAP_so=$'\e[1;45;93m' # Start stand-out effect (similar to reverse text).
export LESS_TERMCAP_se=$'\e[0m'       # Stop stand-out effect (similar to reverse text).

# ------------------------ Environment variables ------------------------

export EDITOR=/usr/bin/vim # Vim, my beloved
export VISUAL=/usr/bin/vim
export TERM=xterm-256color # Colors!

# ------------------------ History ------------------------

### don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

### append to the history file, don't overwrite it
shopt -s histappend

### for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# ------------------------ Aliases ------------------------

[ -f ~/.bash_aliases ] && . ~/.bash_aliases

[ -f ~/.ssh_aliases ] && . ~/.ssh_aliases

[ -f ~/.game_aliases ] && . ~/.game_aliases

# ------------------------ Custom functions ------------------------

### Notebook editing functions
datename=$(date +"%m-%d-%Y").txt
nn() { vim ~/notebook/"$*-${datename}"; }
nls() { ls -t ~/notebook/ | grep "$*"; }
nrm() { rm ~/notebook/"$*"; }
nfind() { find  ~/notebook/ -iname *$**; }
nread() { less $(grep -C 1 -l "$1" ~/notebook/*); }

### Change up a variable number of directories (from BasilGuo: https://github.com/BasilGuo/dotfiles/blob/master/.bashrc)
# E.g:
#   cu   -> cd ../
#   cu 2 -> cd ../../
#   cu 3 -> cd ../../../
function cu {
    local count=$1
    if [ -z "${count}" ]; then
        count=1
    fi
    local path=""
    for i in $(seq 1 ${count}); do
        path="${path}../"
    done
    cd $path
}

### ARCHIVE EXTRACTION (from Derek Taylor https://gitlab.com/dwt1/dotfiles/-/blob/master/.bashrc)
# usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# ------------------------ Other ------------------------

### BASH INSULTER ###
if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi

# -------------- Greeting on new terminal: --------------

### Simple text greeting:
echo
echo -e "\e[1;94mWelcome, $USER.\e[0;30m"
fortune -s

