# https://codeberg.org/Owlrooster/dotfiles/_edit/main/bash/.bash_aliases                   
# ------------------
# Practical utilities
# __________________

# Tweaked utilities
alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias la='ls -a'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias diff='diff --color=auto'
alias cmatrix='cmatrix -b -C blue'
alias df='df -h'					# human-readable sizes
alias free='free -m'				# show sizes in MB

# Git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'

# get error messages from journalctl
alias errorctl="journalctl -p 3 -xb"

# Confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# Memory aids and corrections
alias fzf='fzy'
alias aliases='vim ~/.bash_aliases' # Edit this file

# Movement
alias ..='cd ..'
alias ...='cd ../..'
alias cd.='cd ..'
alias cd..='cd ..'

# Varies by distro; check for Arch; assume Debian if none of the above
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
else
    OS=lsb_release -i | cut -f 2-
fi
# Package manager aliases
if [ "$OS" = "Fedora" ] || [ "$OS" = "Red Hat Enterprise Linux" ]; then
    alias killorphans='sudo apt-get autoremove' # Clean up packages
    alias upd='sudo apt-get update && sudo apt-get upgrade' # Update time!
    alias inst='sudo apt-get install' # Install argument packages.
    alias remv='sudo apt-get remove' # Remove argument packages.
    alias search='apt search' # Find a package
elif [ "$OS" = "Arch Linux" ] || [ "$OS" = "EndeavourOS Linux" ]; then
    alias killorphans='sudo pacman -Rns $(pacman -Qdtq)' # Clean up packages
    alias upd='yay -Syu && paccache -r' # Update time!
    alias inst='yay -Syu' # Install argument packages.
    alias remv='sudo pacman -R' # Remove argument packages.
    alias search='pacman -Ss' # Find a package
    alias aurs='yay -Ss' # Find an AUR package
else
    alias killorphans='sudo apt-get autoremove' # Clean up packages
    alias upd='sudo apt-get update && sudo apt-get upgrade' # Update time!
    alias inst='sudo apt-get install' # Install argument packages.
    alias remv='sudo apt-get remove' # Remove argument packages.
    alias search='apt search' # Find a package
fi

alias clearcache='sync ; echo 3 | sudo tee /proc/sys/vm/drop_caches' # Sometimes the RAM cache has to go.
alias remake='make clean; make' # What it says on the tin.

# Brightness CLI for if the buttons don't work. Comes in handy sometimes.
alias fb='brightnessctl set 255' # Full brightness.
alias bl='brightnessctl set 75' # Low brightness (but not black!)
alias nl='brightnessctl set 0' # No brightness. Screen off.

