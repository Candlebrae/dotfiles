#!/bin/bash

#if anything breaks, see http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

# List of directories/files to scan; replace if desired
scanDir=~/.clamscan-list.txt
# Location to write logfile to; again, substitute as desired
logFile=~/.clamlog.txt
# Recursive or no? 1 for recursion, anything else for no recursion
recursion=1

# Ensure log file exists
touch $logFile
# Perform the scan
if [ $recursion = 1 ]; then
  clamscan -i -r --file-list=$scanDir > $logFile
else
  clamscan -i --file-list=$scanDir > $logFile
fi
# Give the user the results
notify-send -a ClamAV --icon=dialog-information -u critical "ClamAV Scan Results" "$(tail -n 8 $logFile)"
exit 0
